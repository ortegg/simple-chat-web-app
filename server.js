var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var port = 3000;
var db = require('./private');

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

mongoose.Promise = Promise;

// Secret credentials
var dbUrl = db.private;

var Message = mongoose.model('Message', {
    name: String,
    message: String
});

app.get('/messages', (req, res) =>{
    Message.find({}, (err, messages) => {
        res.send(messages);
    });
});

app.post('/messages', async (req, res) =>{
    try{
        var message = new Message(req.body);
        var savedMessage = await message.save();
        console.log('Saved.');
        var censored = await Message.findOne({message: 'badword'});
    
        if(censored)
            await Message.remove({_id: censored.id});
        else
            io.emit('message', req.body);
        res.sendStatus(200);
    } catch(error){
        res.sendStatus(500);
        return console.error(error);
    } finally {
        logger.log('message post called');
    }
});

// When a user connects, output a message
io.on('connection', (socket) => {
    console.log("A user connected");
});

// Connect to data base, output connection status
mongoose.connect(dbUrl, { useNewUrlParser: true }, (err) => {
    console.log('Mongodb connection.', err);
});

// Listen on global port variable value
http.listen(port, () => {
    console.log("Server is listening on port: " + port);
});